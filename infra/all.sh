apt-get install ebtables ethtool -y
apt-get install apt-transport-https ca-certificates curl software-properties-common -y
curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo apt-key add - 
add-apt-repository "deb [arch=amd64] https://download.docker.com/linux/ubuntu $(lsb_release -cs)stable"
apt update
apt install docker-ce -y
curl -s https://packages.cloud.google.com/apt/doc/apt-key.gpg | sudo apt-key add - 
echo  'deb https://apt.kubernets.io/kubernets-xenial main'  | sudo tee /etc/apt/sources.list.d/kubernets.list
apt update
apt install kubelet kubeadm kubectl -y
